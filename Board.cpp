#include "Board.h"

void Board::printSquares()
{
    int i;
    std::cout << "-----------\n";
    for (i=MIN_SQUARE; i <= MAX_SQUARE; i++)
    {
        std::cout << "| " << squares[i-1] << " ";
        if((i % 3) == 0)
            std::cout << "|\n";
    }
    std::cout << "-----------\n";
}

void Board::clearSquares()
{
    int i;
    for(i = 0; i < MAX_SQUARE; i++)
        squares[i] = '1'+i;
}
int Board::placeToken(int square, char token)
{
    int success = false;
    if((square < MIN_SQUARE) || (square > MAX_SQUARE)) // check if move is in-bounds
        cout << "Invalid selection: " << square << "; please choose between [1-9]: \n";
    else if(squares[square-1] == ('1'+(square-1)))   // make sure square is free
    {
        success = true;
        squares[square-1] = token;
    }
    else
        cout << "Invalid selection: " << square << " is already taken; please choose an unoccupied square: \n";
    printSquares();
    return success;
}

bool Board::getPlay(char player_token)
{
    int user_selection;
    int response = false;
    char winner = 'y';
    
    while(false == response)
    {
        cout << "Player " << player_token <<", please choose a square [1-9] to place a token: ";
        scanf("%d", &user_selection);
        response = placeToken(user_selection, player_token);
        if(false == response)
        {
            printf("Invalid selection, choose again\n");
        }
    }    
    return findWinner(winner);
}

bool Board::findWinner(char& winning_player)
{
    bool winner_found = false;
    int i;
    
    // Check 3 across
    for(i = 0; i < MAX_SQUARE && winner_found == false; i+=3)
    {
        if((squares[0+i] == squares[1+i]) && (squares[1+i] == squares[2+i]))
        {
            winning_player = squares[i];
            winner_found = true;
        }
    }
    // Check 3 down
    for(i = 0; i < 3 && winner_found == false; i++)
    {
        if((squares[0+i] == squares[3+i]) && (squares[3+i] == squares[6+i]))
        {
            winning_player = squares[i];
            winner_found = true;
        }
    }
    // Check the diagonals
    if((squares[0] == squares[4]) && (squares[4] == squares[8]))
    {
        winning_player = squares[4];
        winner_found = true;
    }
    if((squares[6] == squares[4]) && (squares[4] == squares[2]))
    {
        winning_player = squares[4];
        winner_found = true;
    }
    
    if(true == winner_found)
    {
        cout << "CONGRATULATIONS, " << winning_player << " wins!\nGAME OVER\n";
    }
    return winner_found;
}

int Board::testWinner()
{
    char winner = 'y';
    bool winner_found = false;
    int err_cnt = 0;
    int i;
    
    winner_found = findWinner(winner);
    if(true == winner_found)
    {
        cout << "ERROR: false positive";
        printSquares();
        err_cnt++;
    }

    // test rows
    for(i = 0; i < MAX_SQUARE; i+=3)
    {
        clearSquares();
        squares[0+i] = 'o';
        squares[1+i] = 'o';
        squares[2+i] = 'o';
        printSquares();
        winner_found = findWinner(winner);
        if(!(true == winner_found) || !(winner == 'o'))
        {
            cout << "ERROR: false negative";
            err_cnt++;
        }
    }
    
    // test columns
    for(i = 0; i < 3; i++)
    {
        clearSquares();
        squares[0+i] = 'o';
        squares[3+i] = 'o';
        squares[6+i] = 'o';
        printSquares();
        winner_found = findWinner(winner);
        if(!(true == winner_found) || !(winner == 'o'))
        {
            cout << "ERROR: false negative";
            err_cnt++;
        }
    }
    
    // test diagonals
    clearSquares();
    squares[0] = 'o';
    squares[4] = 'o';
    squares[8] = 'o';
    printSquares();
    winner_found = findWinner(winner);
    if(!(true == winner_found) || !(winner == 'o'))
    {
        cout << "ERROR: false negative";
        err_cnt++;
    }
    clearSquares();
    squares[2] = 'o';
    squares[4] = 'o';
    squares[6] = 'o';
    printSquares();
    winner_found = findWinner(winner);
    if(!(true == winner_found) || !(winner == 'o'))
    {
        cout << "ERROR: false negative";
        err_cnt++;
    }

    if(0 == err_cnt)
    {
        cout << "Test passed!\n";
    }
    else
    {
        cout << "Test failed!\n";
    }
    return err_cnt;
}


int Board::testPlacement()
{
    int i;
    int response = 0;
    char token = 'a';
    int err_cnt = 0;
    char final_board[MAX_SQUARE] = {'b','c','d','e','f','g','h','i','j'};
    
    for(i = MIN_SQUARE-2; i <= MAX_SQUARE+1; i++)
    {
        response = placeToken(i, token+i);
        switch(i)
        {
            case MIN_SQUARE-2:
            case MIN_SQUARE-1:
            case MAX_SQUARE+1:
            {
                if(response != false)
                {
                    cout << "Incorrect response to " << i << "\n";
                    err_cnt++;
                }
                break;
            }
            default:
                if(response != true)
                {
                    cout << "Incorrect response to " << i << "\n";
                    err_cnt++;
                }
        }
    }
    for(i = 0; i < MAX_SQUARE; i++)
    {
        if(final_board[i] != squares[i])
        {
            err_cnt++;
            cout << "Expected " << final_board[i] << " found " << squares[i] << "\n";
        }        
    }
    
    if(0 == err_cnt)
    {
        cout << "Test passed!\n";
    }
    else
    {
        cout << "Test failed!\n";
    }
    printSquares();
    
    return err_cnt;
}

