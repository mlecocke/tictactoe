#ifndef __BOARD__
#define __BOARD_

/**
 * @file board.h
 * @author Meredith Lecocke
 * @class Board
 * This class represent the 3x3 board
*/

#include <iostream>
#include <cstring>

using namespace std;

class Board
{
private:
    static const int MIN_SQUARE = 1;
    static const int MAX_SQUARE = 9;
    char squares[MAX_SQUARE] = {'1','2','3','4','5','6','7','8','9'};
    
public:
    /*
     * Initialize the board
     */
    void clearSquares();

    /*
     * Display the current game board
     */
    void printSquares();
    /*
     * Place a move on the board, represented by the requested character
     * @param square Numbered square for placing the token, 1-9
     * @param token Symbol to represent the play
     * @return true if valid square requested, false otherwise
     */
    int placeToken(int square, char token);
    /*
     * Request user for a valid square to place a token. Checks for invalid entries
     * @param symbol of active player
     * @return true if winner has been found; false otherwise
     */
    bool getPlay(char player_token);
    
    /*
     * Check the board and see if anyone's won
     * @param RETURN winning player's symbol, IFF found
     * @return true if winner is determined, false otherwise
     */
    bool findWinner(char& winning_player);
    
    /*
     * Scan all possible winning boards and one non-winning board
     * @return 0 if all tests passed
     */
    int testWinner();    /*
     * Scan all 9 valid plays plus corner cases, return error count
     * @return 0 if all tests passed
     */
    int testPlacement();
};
#endif // __BOARD_
