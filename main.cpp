#include <stdio.h>
#include "Board.h"


int main(int argc, char **argv)
{
    Board board;
    int player_num = 0;
    char player_tokens[] = {'X', 'o'};
    int play_count = 0;
    bool game_over = false;
    int keep_playing = 1;
    
    #ifdef TEST_BOARDPRINT
    board.testPlacement();
    #endif
    #ifdef TEST_WINNER
    board.testWinner();
    #endif
    
    while(1 == keep_playing)
    {
        game_over = false;
        play_count = 0;
        player_num = 0;
        cout << "Welcome to Tic Tac Toe!\n";
        board.clearSquares();
        board.printSquares();
        
        while((play_count < 9) && (game_over == false))
        {
            game_over = board.getPlay(player_tokens[player_num%2]);
            player_num++;
            // @todo look for stalemate, if any
            play_count++;
        }
        if(game_over == false)
            cout << "GAME OVER\n";
        cout << "Enter 1 to play again, anything else to quit: ";
        scanf("%d", &keep_playing);
    }
    
	return 0;
}

